Title: Poste MCF Section CNU 27 (Informatique) 
Date: 2023-02-03 13:20
Slug: job_f71fbe09fd2cb516d420a4e254fcfe4d
Category: job
Author: Stéphan Vialle, Janna Burman
Email: stephane.vialle@lisn.upsaclay.fr
Job_Type: Concours
Tags: concours
Template: job_offer
Job_Location: 
Job_Duration: 
Job_Website: 
Job_Employer: Université Paris-Saclay, IUT d&#39;Orsay
Expiration_Date: 2023-04-28
Attachment: job_f71fbe09fd2cb516d420a4e254fcfe4d_attachment.pdf

**Session synchronisée 2023**
**Profil : Algorithmique distribuée, Calcul à Haute Performance, Théorie structurale des graphes et l’algorithmique**

**Recherche**

La personne recrutée intégrera l’une de deux équipes ParSys ou GALaC du LISN, sans priorité.

*Profil ParSys*

La personne recrutée collaborera avec les membres de l’équipe autour de thèmes parmi les suivants :

- Algorithmique distribuée : tolérance aux défaillances, réplication et consensus, autostabilisation, graphes dynamiques, algorithmes inspirés de la nature, protocoles de populations, agents mobiles.
- Calcul à Haute Performance : calcul scientifique à large échelle, bibliothèques d’algèbre linéaire, algorithmes parallèles de Machine Learning, d’analyse de données, et de simulations de systèmes quantiques.

*Profil GALaC*

La personne recrutée démontrera sa capacité à travailler avec les membres de l’équipe dans un ou plusieurs de ses thèmes, à savoir : la théorie structurelle ou algorithmique des graphes, l’algorithmique des systèmes en réseaux et distribués, la combinatoire énumérative et algébrique et la dynamique symbolique. Une priorité sera donnée aux profils ayant des liens forts avec la théorie structurale des graphes et l’algorithmique. La qualité du dossier prime.

*Qualités globales appréciées*

Les deux équipes ont une tradition de collaboration, et les candidatures pouvant faire du lien entre elles (et avec d&#39;autres équipes du département et du laboratoire) seront appréciés. La personne recrutée s’impliquera dans l’animation scientifique du laboratoire. Un rayonnement international sera également apprécié.

**Enseignement**

Filières de formation concernées : les trois années de BUT informatique et le BUT informatique par apprentissage (parcours A : « Réalisation d’applications : conception, développement, validation » et parcours C : « Administration, gestion et exploitation des données »).

Les enseignements couvrent donc l’algorithmique et la programmation, les bases de données, la programmation Web, les interfaces homme-machine, etc.
